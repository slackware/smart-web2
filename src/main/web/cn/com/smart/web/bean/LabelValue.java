package cn.com.smart.web.bean;

/**
 * 标签与值
 * @author lmq
 *
 */
public class LabelValue {

    private String label;
    
    private String value;

    public LabelValue() {
    }
    
    public LabelValue(String label, String value) {
        this.label = label;
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    
    /**
     * 转换为数组
     * @return
     */
    public String[] toArray() {
        String[] array = new String[2];
        array[0] = this.value;
        array[1] = this.label;
        return array;
    }
    
}
