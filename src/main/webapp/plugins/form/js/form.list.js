/**
 * 创建表
 */
(function($){
	
	/**
	 * 列表字段属性监听
	 */
	$.fn.formListPropListener = function() {
		var $this = $(this);
		var fieldSeqNum = 1;
		var btnSeqNum = 1;
		var cellSeqNum = 1;
		//var fields = [];
		//添加字段
		$(".add-form-list-field").click(function() {
			var $tbody = $this.find("#form-list-field-table tbody");
			if(!utils.isEmpty($tbody.html())) {
				var $lastTr = $tbody.find("tr:last");
				var lastSeqNum = $lastTr.find(".seq-num").text();
				fieldSeqNum = parseInt(lastSeqNum)+1;
			}
			var fieldElementTr = "<tr id=\"tr"+fieldSeqNum+"\"><td class=\"seq-num text-right\" style=\"width: 40px;\">"+
				"<input type=\"hidden\" class=\"sort-order\" name=\"report.fields["+(fieldSeqNum-1)+"].sortOrder\" value=\""+fieldSeqNum+"\" /><span class=\"sort-order-label\">"+fieldSeqNum+"</span></td>"+
				"<td><input id=\"title"+fieldSeqNum+"\" name=\"report.fields["+(fieldSeqNum-1)+"].title\" class=\"form-control title \" placeholder=\"请填写标题，必填\" title=\"必填项\" type=\"text\" /></td>"+
				"<td ><input class=\"form-control\" name=\"report.fields["+(fieldSeqNum-1)+"].width\" type=\"text\" placeholder=\"自动宽度\" /></td>"+
				"<td><input class=\"form-control\" type=\"text\" name=\"report.fields["+(fieldSeqNum-1)+"].url\" placeholder=\"URL地址\" /></td>"+
				"<td><select class=\"form-control cnoj-select\" name=\"report.fields["+(fieldSeqNum-1)+"].openUrlType\" data-uri=\"dict/item/OPEN_URL_TYPE.json\"></select></td>"+
				"<td><input class=\"form-control\" name=\"report.fields["+(fieldSeqNum-1)+"].paramName\" title=\"多个参数用英文逗号分隔，如果没有请为空\" placeholder=\"多个参数用英文逗号分隔，如果没有请为空\" type=\"text\"  /></td>"+
				"<td><input class=\"form-control\" name=\"report.fields["+(fieldSeqNum-1)+"].paramValue\" title=\"多个参数引用下标用英文逗号分隔，如果没有请为空\" placeholder=\"多个参数引用下标用英文逗号分隔，如果没有请为空\" type=\"text\"  /></td>"+
				"<td><input class=\"form-control\" name=\"report.fields["+(fieldSeqNum-1)+"].searchName\" title=\"填写搜索变量，如果该标题不是搜索项，请为空\" placeholder=\"填写搜索变量，如果该标题不是搜索项，请为空\" type=\"text\"  /></td>"+
				"<td><select class=\"form-control cnoj-select\" name=\"report.fields["+(fieldSeqNum-1)+"].searchPluginType\" data-uri=\"form/list/searchPluginType.json\"><option value=\"\">请选择</option></select></td>"+
				"<td><input name=\"report.fields["+(fieldSeqNum-1)+"].searchPluginUrl\" class=\"form-control\"  title=\"填写搜索插件数据来源，如果搜索插件没有数据来源，请为空\" placeholder=\"填写搜索插件数据来源，如果搜索插件没有数据来源，请为空\" type=\"text\" /></td>"+
				"<td><textarea name=\"report.fields["+(fieldSeqNum-1)+"].searchCustomOptions\" class=\"form-control field-textarea\"  title=\"搜索选项值\" placeholder=\"搜索选项值\" cols='1' ></textarea></td>"+
				"<td><input name=\"report.fields["+(fieldSeqNum-1)+"].sortFieldName\" class=\"form-control\"  title=\"填写按该标题排序的字段名，如果该标题不支持排序，请为空\" placeholder=\"填写按该标题排序的字段名，如果该标题不支持排序，请为空\" type=\"text\" /></td>"+
				"<td class=\"del-td\" id=\"del-field"+fieldSeqNum+"\" style=\"width: 40px;\" class=\"text-center\"></td>"+
            "</tr>";
			$tbody.append(fieldElementTr);
            $("<button type=\"button\" title=\"删除\" class=\"delete-field text-center\" style=\"float: none;font-size: 18px;\" data-dismiss=\"tr1\" aria-label=\"Close\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i></button>").click(function(){
                $(this).parent().parent().remove();
				fieldResort();
            }).appendTo("#del-field"+fieldSeqNum);
            //var selectFieldId = '#select-field-'+fieldSeqNum;
            selectListener($this);
			//utils.selectDataItem(selectFieldId, fields, null, null);
		});

		//删除字段
		$(".delete-field").click(function() {
			$(this).parent().parent().remove();
			fieldResort();
		});
		
		/**
		 * 重新排序
		 */
		function fieldResort() {
		  //重新排序
            var index = 1;
            var $tbody = $this.find("#form-list-field-table tbody");
            $tbody.find("tr").each(function() {
                var $tr = $(this);
                $tr.attr("id", "tr"+index);
                $tr.find(".sort-order").val(index);
                $tr.find(".sort-order-label").text(index);
                $tr.find(".del-td").attr("id","del-field"+index);
                $tr.find("input,select").each(function() {
                    var $element = $(this);
                    var name = $element.attr("name");
                    name = name.replace(/report\.fields\[\d+\]\./, "report.fields["+(index-1)+"].");
                    $element.attr("name", name);
                });
                index++;
            });
		}

		//添加单元格
		$(".add-cell").click(function() {
			var $tbody = $this.find("#form-list-cell-table tbody");
			if(!utils.isEmpty($tbody.html())) {
				var $lastTr = $tbody.find("tr:last");
				var lastSeqNum = $lastTr.find(".seq-num").text();
				cellSeqNum = parseInt(lastSeqNum)+1;
			}
			var fieldElementTr = "<tr id=\"tr"+cellSeqNum+"\"><td class=\"seq-num text-right\" style=\"width: 40px;\"><span class=\"sort-order-label\">"+cellSeqNum+"</span></td>"+
				"<td><input name=\"report.customCells["+(cellSeqNum-1)+"].position\" class=\"form-control\" placeholder=\"请输入显示的位置序号，从1开始\" title=\"请输入显示的位置序号，从1开始\" type=\"text\" /></td>"+
				"<td ><textarea class=\"form-control field-textarea\" name=\"report.customCells["+(cellSeqNum-1)+"].content\" type=\"text\" title='请输入显示的内容模板' placeholder=\"请输入显示的内容模板\"></textarea></td>"+
				"<td><input class=\"form-control\" name=\"report.customCells["+(cellSeqNum-1)+"].paramName\" title=\"内容模板中出现的参数名，多个用英文逗号分隔\" placeholder=\"内容模板中出现的参数名，多个用英文逗号分隔\" type=\"text\" /></td>"+
				"<td><input class=\"form-control\" name=\"report.customCells["+(cellSeqNum-1)+"].paramValue\" title=\"参数名对应值的数据下标，从0开始\" placeholder=\"参数名对应值的数据下标，从0开始\" type=\"text\" /></td>"+
				"<td class=\"del-td\" id=\"del-cell"+cellSeqNum+"\" class=\"text-center\"></td>"+
				"</tr>";
			$tbody.append(fieldElementTr);
			$("<button type=\"button\" title=\"删除\" class=\"delete-cell text-center\" style=\"float: none;font-size: 18px;\" data-dismiss=\"tr1\" aria-label=\"Close\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i></button>").click(function(){
				$(this).parent().parent().remove();
				cellResort();
			}).appendTo("#del-cell"+cellSeqNum);
		});

		//删除字段
		$(".delete-cell").click(function() {
			$(this).parent().parent().remove();
			cellResort();
		});

		/**
		 * 重新排序
		 */
		function cellResort() {
			//重新排序
			var index = 1;
			var $tbody = $this.find("#form-list-cell-table tbody");
			$tbody.find("tr").each(function(){
				var $tr = $(this);
				$tr.attr("id", "tr"+index);
				$tr.find(".sort-order").val(index);
				$tr.find(".sort-order-label").text(index);
				$tr.find(".del-td").attr("id","del-cell"+index);
				$tr.find("input,select").each(function(){
					var $element = $(this);
					var name = $element.attr("name");
					name = name.replace(/report\.customCells\[\d+\]\./, "report.customCells["+(index-1)+"].");
					$element.attr("name", name);
				});
				index++;
			});
		}

		//添加按钮
		$(".add-btn").click(function() {
			var $tbody = $this.find("#form-list-btn-table tbody");
			if(!utils.isEmpty($tbody.html())) {
				var $lastTr = $tbody.find("tr:last");
				var lastSeqNum = $lastTr.find(".seq-num").text();
				btnSeqNum = parseInt(lastSeqNum)+1;
			}
			var fieldElementTr = "<tr id=\"tr"+btnSeqNum+"\"><td class=\"seq-num text-right\" style=\"width: 40px;\">"+
				"<input type=\"hidden\" class=\"sort-order\" name=\"report.buttons["+(btnSeqNum-1)+"].sortOrder\" value=\""+btnSeqNum+"\" /><span class=\"sort-order-label\">"+btnSeqNum+"</span></td>"+
				"<td><input name=\"report.buttons["+(btnSeqNum-1)+"].name\" class=\"form-control\" placeholder=\"请填写按钮名称，必填\" title=\"必填项\" type=\"text\" /></td>"+
				"<td><select name=\"report.buttons["+(btnSeqNum-1)+"].btnStyle\" class=\"form-control\">"+
				"<option value=\"btn-default\">默认</option><option value=\"btn-primary\" style=\"background-color: #428bca;\">主要</option>"+
				"<option value=\"btn-success\" style=\"background-color: #449d44;\">成功</option><option value=\"btn-info\" style=\"background-color: #5bc0de;\">信息</option>"+
				"<option value=\"btn-warning\" style=\"background-color: #f0ad4e;\">警告</option><option value=\"btn-danger\" style=\"background-color: #d9534f;\">危险</option>"+
				"</td>"+
				"<td><select name=\"report.buttons["+(btnSeqNum-1)+"].btnId\" class=\"form-control cnoj-select\" data-uri=\"opauth/options\"></select></td>"+
				"<td ><input class=\"form-control\" name=\"report.buttons["+(btnSeqNum-1)+"].url\" type=\"text\" placeholder=\"点击按钮时触发的URL地址\" /></td>"+
				"<td><select class=\"form-control cnoj-select\" name=\"report.buttons["+(btnSeqNum-1)+"].selectedType\" data-uri=\"web/enum/btn/selectedListType.json\"></select></td>"+
				"<td><select class=\"form-control cnoj-select\" name=\"report.buttons["+(btnSeqNum-1)+"].openType\" data-uri=\"web/enum/btn/openType.json\"></select></td>"+
				"<td><select class=\"form-control cnoj-select\" name=\"report.buttons["+(btnSeqNum-1)+"].isAuth\" data-uri=\"dict/item/YES_OR_NO.json\"></select></td>"+
				"<td><input class=\"form-control\" name=\"report.buttons["+(btnSeqNum-1)+"].dialogTitle\" title=\"填写弹出框或tab的标题，如果不是弹出框或tab，请为空\" placeholder=\"填写弹出框或tab的标题，如果不是弹出框或tab，请为空\" type=\"text\" /></td>"+
				"<td><input class=\"form-control\" name=\"report.buttons["+(btnSeqNum-1)+"].dialogWidth\" title=\"填写弹出框窗口的宽度，如果不是弹出框窗口，请为空\" placeholder=\"填写弹出框窗口的宽度，如果不是弹出框窗口，请为空\" type=\"text\" /></td>"+
				"<td><input class=\"form-control\" name=\"report.buttons["+(btnSeqNum-1)+"].btnIcon\" title=\"填写按钮的图标，请使用bootstrap的字体图标，可为空\" placeholder=\"填写按钮的图标，请使用bootstrap的字体图标，可为空\" type=\"text\" /></td>"+
				"<td><input class=\"form-control\" name=\"report.buttons["+(btnSeqNum-1)+"].paramName\"   title=\"填写参数名称，默认为“id”，如果没有选择的值，请为空\" placeholder=\"填写参数名称，默认为“id”，如果没有选择的值，请为空\" type=\"text\" /></td>"+
				"<td><input class=\"form-control\" name=\"report.buttons["+(btnSeqNum-1)+"].promptMsg\" type=\"text\" placeholder=\"用于删除提醒\" /></td>"+
				"<td class=\"del-td\" id=\"del-btn"+btnSeqNum+"\" style=\"width: 40px;\" class=\"text-center\"></td>"+
				"</tr>";
			$tbody.append(fieldElementTr);
			$("<button type=\"button\" title=\"删除\" class=\"delete-btn text-center\" style=\"float: none;font-size: 18px;\" data-dismiss=\"tr1\" aria-label=\"Close\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i></button>").click(function(){
				$(this).parent().parent().remove();
				btnResort();
			}).appendTo("#del-btn"+btnSeqNum);
			selectListener($this);
		});

		//删除字段
		$(".delete-btn").click(function() {
			$(this).parent().parent().remove();
			btnResort();
		});

		/**
		 * 重新排序
		 */
		function btnResort() {
			//重新排序
			var index = 1;
			var $tbody = $this.find("#form-list-btn-table tbody");
			$tbody.find("tr").each(function(){
				var $tr = $(this);
				$tr.attr("id", "tr"+index);
				$tr.find(".sort-order").val(index);
				$tr.find(".sort-order-label").text(index);
				$tr.find(".del-td").attr("id","del-btn"+index);
				$tr.find("input,select").each(function(){
					var $element = $(this);
					var name = $element.attr("name");
					name = name.replace(/report\.buttons\[\d+\]\./, "report.buttons["+(index-1)+"].");
					$element.attr("name", name);
				});
				index++;
			});
		}
		
		/**
		 * 保存报表设计
		 */
		$("#save-form-list-designer").click(function(){
		    if($("#form-list-designer-form").validateForm()) {
		        var formData = $("#form-list-designer-form").serialize();
		        utils.waitLoading("正在提交数据...");
		        $.post('form/list/save', formData, function(response){
		            utils.closeWaitLoading();
		            utils.showMsg(response.msg);
		            if(response.result==1) {
		                closeActivedTab();
						loadLocation('form/list/list');
		            }
		        });
		    }
		});
	}
})(jQuery);