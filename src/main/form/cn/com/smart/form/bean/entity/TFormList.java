package cn.com.smart.form.bean.entity;

import cn.com.smart.bean.BaseBeanImpl;
import cn.com.smart.bean.DateBean;
import cn.com.smart.form.enums.FormListDataScopeType;
import cn.com.smart.form.enums.FormListType;
import cn.com.smart.report.bean.entity.TReport;
import com.mixsmart.enums.YesNoType;

import javax.persistence.*;
import java.util.Date;

/**
 * 表单列表实体
 * @author lmq
 *
 */
@Entity
@Table(name = "t_form_list")
public class TFormList extends BaseBeanImpl implements DateBean {

    private String id;

    private String parentId;

    private String formId;
    
    private String reportId;
    
    private String name;
    
    //数据可见范围；默认为全部可见；详情请查看{@link FormListDataScopeType}
    private String dataScope = FormListDataScopeType.ALL.getValue();

    /**
     * 主表单、子表单；
     */
    private String type = FormListType.MAIN.getValue();

    private Integer isAutoCreate = YesNoType.NO.getIndex();
    
    private Date createTime;
    
    private String userId;

    //非持久化属性
    private TReport report;

    @Id
    @Column(name = "id", length = 50)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Column(name = "parent_id", length = 50)
    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    @Column(name = "type", length = 50, nullable = false)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Column(name = "form_id", length = 50)
    public String getFormId() {
        return formId;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }

    @Column(name = "report_id", length = 50, nullable = false)
    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    @Column(name = "name", length = 255, nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    @Column(name = "data_scope", length = 127, nullable = false)
    public String getDataScope() {
        return dataScope;
    }

    public void setDataScope(String dataScope) {
        this.dataScope = dataScope;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_time", updatable = false)
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Column(name = "user_id", length = 50, nullable = false)
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Column(name = "is_auto_create", nullable = false)
    public Integer getIsAutoCreate() {
        return isAutoCreate;
    }

    public void setIsAutoCreate(Integer isAutoCreate) {
        this.isAutoCreate = isAutoCreate;
    }

    @Transient
    @Override
    public String getPrefix() {
        // TODO Auto-generated method stub
        return "FL";
    }


    @Transient
    public TReport getReport() {
        return report;
    }

    public void setReport(TReport report) {
        this.report = report;
    }

}
