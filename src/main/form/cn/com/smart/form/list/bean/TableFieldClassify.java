package cn.com.smart.form.list.bean;

import java.util.List;

/**
 * @author 乌草坡 2019-09-02
 * @since 1.0
 */
public class TableFieldClassify {

    private String tableId;

    private String tableName;

    private String tableAlias;

    private List<FieldName> fields;

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getTableAlias() {
        return tableAlias;
    }

    public void setTableAlias(String tableAlias) {
        this.tableAlias = tableAlias;
    }

    public List<FieldName> getFields() {
        return fields;
    }

    public void setFields(List<FieldName> fields) {
        this.fields = fields;
    }

    public static class FieldName {

        private String fieldId;

        private String fieldName;

        private String tableAlias;

        public FieldName() {
        }

        public FieldName(String fieldId, String fieldName, String tableAlias) {
            this.fieldId = fieldId;
            this.fieldName = fieldName;
            this.tableAlias = tableAlias;
        }

        public String getFieldId() {
            return fieldId;
        }

        public void setFieldId(String fieldId) {
            this.fieldId = fieldId;
        }

        public String getFieldName() {
            return fieldName;
        }

        public void setFieldName(String fieldName) {
            this.fieldName = fieldName;
        }

        public String getTableAlias() {
            return tableAlias;
        }

        public void setTableAlias(String tableAlias) {
            this.tableAlias = tableAlias;
        }
    }
}
