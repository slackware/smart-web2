package cn.com.smart.form;

/**
 * 表单相关常量定义
 */
public class FormConstants {

    /**
     * 排序方式变量标识
     */
    public static final String LIST_FORM_SORTING = "sorting";

    /**
     * 排序字段变量标识
     */
    public static final String LIST_FORM_SORT_FIELD = "sortField";

    /**
     * 定义排序方式 -- 升序
     */
    public static final String SORTING_ASC = "asc";

    /**
     * 定义排序方式 -- 降序
     */
    public static final String SORTING_DESC = "desc";
}
