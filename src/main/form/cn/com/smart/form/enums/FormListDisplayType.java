package cn.com.smart.form.enums;

import java.util.ArrayList;
import java.util.List;

import com.mixsmart.utils.StringUtils;

import cn.com.smart.web.bean.LabelValue;

/**
 * 定义表单列表的展示类型
 * @author lmq
 *
 */
public enum FormListDisplayType {

    TABLE("table", "普通表格"),
    TREE_TABLE("tree_table", "树型表格");
    
    private String value;
    
    private String text;

    private FormListDisplayType(String value, String text) {
        this.value = value;
        this.text = text;
    }

    /**
     * 获取列表展现类型对象
     * @param value 类型值
     * @return 返回类型对象
     */
    public static FormListDisplayType getObj(String value) {
        if(StringUtils.isEmpty(value)) {
            return null;
        }
        FormListDisplayType type = null;
        for(FormListDisplayType typeTmp : FormListDisplayType.values()) {
            if(typeTmp.getValue().equals(value)) {
                type = typeTmp;
                break;
            }
        }
        return type;
    }
    
    /**
     * 获取选项列表
     * @return 返回选项列表
     */
    public static List<LabelValue> getOptions() {
        List<LabelValue> labelValues = new ArrayList<LabelValue>();
        for(FormListDisplayType typeTmp : FormListDisplayType.values()) {
            labelValues.add(new LabelValue(typeTmp.getText(), typeTmp.getValue()));
        }
        return labelValues;
    }
    
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
    
    
    
    
}
