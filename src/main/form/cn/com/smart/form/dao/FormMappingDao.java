package cn.com.smart.form.dao;

import cn.com.smart.dao.impl.BaseDaoImpl;
import cn.com.smart.form.bean.entity.TFormMapping;
import org.springframework.stereotype.Repository;

/**
 * @author 乌草坡 2019-09-15
 * @since 1.0
 */
@Repository
public class FormMappingDao extends BaseDaoImpl<TFormMapping> {
}
